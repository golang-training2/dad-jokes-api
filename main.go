package main

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"
)

type DadJoke struct {
	ID     string `json:"id"`
	Joke   string `json:"joke"`
	Status int    `json:"status"`
}

func main() {
	app := fiber.New()

	app.Get("/dadjoke", getDadJoke)

	app.Listen(":3000")
}

func getDadJoke(c *fiber.Ctx) error {
	var apiResp DadJoke
	getDadJokeFromApi(&apiResp)
	response, err := json.Marshal(apiResp)
	if err != nil {
		panic(err)
	}
	return c.SendString(string(response))
}

func getDadJokeFromApi(resp *DadJoke) {
	a := fiber.Get("https://icanhazdadjoke.com").Set("Accept", "application/json")
	if err := a.Parse(); err != nil {
		panic(err)
	}
	a.Struct(resp)
}
