# Dad Jokes API
## Setup Project
1. Clone project with `git clone https://gitlab.com/golang-training2/dad-jokes-api.git`
2. Run `go get`
3. start with `go run main.go`
4. open in ` http://127.0.0.1:3000` or the addres spesified on app.Listen function

## Documentation
The api documentation can be seen here  
[https://docs.google.com/document/d/10IKueqxLoeIlLci2Y5TXsrJaDArjNA1N7zpa5-CNpW0/edit?usp=sharing](https://docs.google.com/document/d/10IKueqxLoeIlLci2Y5TXsrJaDArjNA1N7zpa5-CNpW0/edit?usp=sharing)